# brkstn/Version

> Get the version number from any .json file in your project.

## Install

```sh
$ composer require brkstn/version:~1.0.0
```

## Usage

Since most projects have both the ``vendor`` and ``package.json`` directory in the project root.  The default arguments are the parent directory of ``vendor`` and ``package.json``, but any valid .json file with a ``version`` property on the top level is acceptable.

```php
// Using default parameters (parent_directory_of_vendor, package.json)
$versionLoader = new brkstn\Version();
$version = $versionLoader->number;

// Setting filepath and filename
$versionLoader = new brkstn\Version(dirname(__DIR__), "composer.json");
$version = $versionLoader->number;
```
