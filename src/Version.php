<?php

namespace brkstn;

/**
 * This is the Version class.
 *
 * It's responsible for loading a `*.json` file and returning the version property value.
 */
class Version
{
    /**
     * The file path.
     *
     * @var string
     */
    protected $filePath;

    /**
     * The loader instance.
     *
     * @var \brkstn\Loader|null
     */
    protected $loader;

    /**
     * The default file.
     *
     * @var string
     */
    protected $default = 'package.json';

    /**
     * The version number.
     *
     * @var string
     */
    public $number;

    /**
     * Create a new version instance.
     *
     * @param string $path
     * @param string $file
     *
     * @return void
     */
    public function __construct($path = null, $file = null)
    {
        if($file === null) $file = $this->default;
        if($path === null) $path = dirname(__DIR__) . "/../../../";

        $this->filePath = $this->getFilePath($path, $file);
        $this->loader = new Loader($this->filePath, true);

        $this->number = $this->load();
    }

    /**
     * Load file in given directory.
     *
     * @return array
     */
    public function load()
    {
        return $this->loadData();
    }

    /**
     * Load file in given directory.
     *
     * @return array
     */
    public function overload()
    {
        return $this->loadData(true);
    }

    /**
     * Returns the full path to the file.
     *
     * @param string $path
     * @param string $file
     *
     * @return string
     */
    protected function getFilePath($path, $file)
    {
        if (!is_string($file)) {
            $file = $this->default;
        }

        $filePath = rtrim($path, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.$file;

        return $filePath;
    }

    /**
     * Actually load the data.
     *
     * @param bool $overload
     *
     * @return array
     */
    protected function loadData($overload = false)
    {
        $this->loader = new Loader($this->filePath, !$overload);

        return $this->loader->load();
    }
}
