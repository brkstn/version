<?php

namespace brkstn\Exception;

use RuntimeException;

/**
 * This is the validation exception class.
 */
class ValidationException extends RuntimeException implements ExceptionInterface
{
    //
}
