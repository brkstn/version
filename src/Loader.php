<?php

namespace brkstn;

use brkstn\Exception\InvalidFileException;
use brkstn\Exception\InvalidPathException;

/**
 * This is the loader class.
 *
 * It's responsible for loading the version by reading a file from disk and:
 * - parsing a json file for a version property and returning it's value as a string.
 */
class Loader
{
    /**
     * The file path.
     *
     * @var string
     */
    protected $filePath;

    /**
     * Are we immutable?
     *
     * @var bool
     */
    protected $immutable;

    /**
     * Create a new loader instance.
     *
     * @param string $filePath
     * @param bool   $immutable
     *
     * @return void
     */
    public function __construct($filePath, $immutable = false)
    {
        $this->filePath = $filePath;
        $this->immutable = $immutable;
    }

    /**
     * Load `*.json` file in given directory.
     *
     * @return array
     */
    public function load()
    {
        $this->ensureFileIsReadable();

        $filePath = $this->filePath;
        $version = $this->parseFile($filePath);

        return $version;
    }

    /**
     * Ensures the given filePath is readable.
     *
     * @throws \brkstn\Exception\InvalidPathException
     *
     * @return void
     */
    protected function ensureFileIsReadable()
    {
        if (!is_readable($this->filePath) || !is_file($this->filePath) ) {
            throw new InvalidPathException(sprintf('Unable to read the file at %s.', $this->filePath));
        }
    }

    /**
     * Wrapper for json_decode that throws when an error occurs.
     *
     * @param string $json    JSON data to parse
     * @param bool $assoc     When true, returned objects will be converted
     *                        into associative arrays.
     * @param int    $depth   User specified recursion depth.
     * @param int    $options Bitmask of JSON decode options.
     *
     * @return mixed
     * @throws \InvalidArgumentException if the JSON cannot be decoded.
     * @link http://www.php.net/manual/en/function.json-decode.php
     */
    protected function jsonDecode($json, $assoc = false, $depth = 512, $options = 0)
    {
        $data = \json_decode($json, $assoc, $depth, $options);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new \InvalidArgumentException(
                'json_decode error: ' . json_last_error_msg());
        }

        return $data;
    }

    /**
     * Parse json from the file.
     *
     * @param string $filePath
     *
     * @return array
     */
    protected function parseFile($filePath)
    {
        $data = $this->jsonDecode(file_get_contents($filePath));

        if(property_exists($data, 'version')) {
            return trim($data->version);
        }

        return false;
    }
}
